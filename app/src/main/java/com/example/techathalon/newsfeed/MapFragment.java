package com.example.techathalon.newsfeed;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.techathalon.newsfeed.Database.DatabaseHelper;
import com.example.techathalon.newsfeed.Database.Login;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapFragment extends Fragment implements OnMapReadyCallback {


    MapView mapview;
    View rootview;
    DatabaseHelper databaseHelper;
    private SharedPrefConfig prefConfig;
    double latt = 0.0, longi = 0.0;
    private GoogleMap mMap;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_map, container, false);


        databaseHelper = new DatabaseHelper(getContext());
        prefConfig = new SharedPrefConfig(getContext());

//        if (!prefConfig.readLoginStatus()) {
//            getFragmentManager().beginTransaction().replace(R.id.framelayout, new Login()).commit();
//            getActivity().finish();
//        }
        String val1 = prefConfig.getlatt();
        String val2 = prefConfig.getLong();

        if (val1 == null && val2 == null) {
            latt = 0.0;
            longi = 0.0;
        } else {
            latt = Double.parseDouble(val1);
            longi = Double.parseDouble(val2);
        }
        return rootview;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mapview = (MapView) rootview.findViewById(R.id.mapview);
        if (mapview != null) {
            mapview.onCreate(null);
            mapview.onResume();
            mapview.getMapAsync(this);

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
//        if (googleMap != null) {
//            return;
//        }

        mMap = googleMap;

        //  zoom levels
//        1: World
//        5: Landmass/continent
//        10: City
//        15: Streets
//        20: Buildings

        // Add a marker in Sydney and move the camera

        if (latt == 0.0 || longi == 0.0) {
            LatLng sydney = new LatLng(19.167419, 72.936247);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Default Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));

        } else {
            LatLng sydney = new LatLng(latt, longi);
            mMap.addMarker(new MarkerOptions().position(sydney).title("My Location is Here"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
        }

    }
}