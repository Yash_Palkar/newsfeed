package com.example.techathalon.newsfeed.Adapters;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.techathalon.newsfeed.GetNews.NewsHome;

import java.util.ArrayList;


public class FinalAdapter extends FragmentStatePagerAdapter {

    ArrayList<String> newsLinks;
    ArrayList<String> newsTitle;

    public FinalAdapter(FragmentManager fm, ArrayList<String> newsLinks, ArrayList<String> newsTitle) {
        super(fm);
        this.newsLinks = newsLinks;
        this.newsTitle = newsTitle;
    }

    @Override
    public Fragment getItem(int position) {

        NewsHome newsHome = new NewsHome();
        Bundle bundle = new Bundle();
        bundle.putString("Source_Url", newsLinks.get(position));
        newsHome.setArguments(bundle);
        return newsHome;
    }

    public int getItemPosition(int position) {
       newsLinks.get(position);
        return position;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return newsTitle.get(position);
    }

    @Override
    public int getCount() {
        return newsLinks.size();
    }

}
