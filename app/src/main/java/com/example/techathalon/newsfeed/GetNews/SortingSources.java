package com.example.techathalon.newsfeed.GetNews;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.techathalon.newsfeed.Home;
import com.example.techathalon.newsfeed.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SortingSources extends AppCompatActivity implements View.OnClickListener {

    TextView selectCountry, selectCategory, selectLanguage;
    ListView listView;
    Button cancelButton, applyButton, clear;

    String countryItem, categoryItem, languageItem;

    private static final String API_KEY = "326b273af74c4a80913bca7789f2d898";

    public static String baseurl = "https://newsapi.org/v2/sources?&apiKey=" + API_KEY;

    String[] category = {"Business", "Entertainment", "General", "Health", "Science", "Sports", "Technology"};
    String[] categoryOrig = {"business", "entertainment", "general", "health", "science", "sports", "technology"};

    String[] languageORIG = {"ar", "de", "en", "es", "fr", "he", "it", "nl", "no", "pt", "ru", "se", "ud", "zh"};

    String[] language = {"Arab", "Germany", "English", "Spanish", "French",
            "Hebrew", "Italian", "Dutch", "Norwegian", "Portuguese",
            "Russian", "Northern Sami", "Urdu", "Chinese"};

    String[] country = {"United Arab Emirates", "Argentina", "Austria", "Australia", "Belgium", "Bulgaria", "Brazil", "Canada",
            "Switzerland", "China", "Colombia", "Cuba", "Czech Republic", "Germany", "Egypt", "France",
            "United Kingdom", "Greece", "Hong Kong, SAR China", "Hungary", "Indonesia", "Ireland", "Israel", "India",
            "Italy", "Japan", "Korea", "Lithuania", "Latvia", "Morocco", "Federated States of Micronesia", "Malaysia",
            "Nigeria", "Netherlands", "Norway", "New Zealand", "Philippines", "Poland", "Portugal", "Romania",
            "Serbia", "Russian Federation", "Saudi Arabia", "Sweden", "Singapore", "Slovenia", "Slovakia",
            "Thailand",  "Turkey", "Taiwan, Republic of China", "Ukraine", "United States of America",
            "Venezuela", "South Africa"};

    String[] countryOrig = {"ae", "ar", "at", "au",
            "be", "bg", "br", "ca",
            "ch", "cn", "co", "cu", "cz",
            "de", "eg", "fr",
            "gb", "gr", "hk", "hu", "id",
            "ie", "il", "in",
            "it", "jp", "kr", "lt", "lv",
            "ma", "mx", "my",
            "ng", "nl", "no", "nz", "ph",
            "pl", "pt", "ro",
            "rs", "ru", "sa", "se", "sg",
            "si", "sk", "th",
            "tr", "tw", "ua", "us", "ve", "za"};

    List<Sources> categoryJson;
    List<Sources> languageJson;
    List<Sources> countryJson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sorting_sources);

        listView = (ListView) findViewById(R.id.filterOptions);
        cancelButton = (Button) findViewById(R.id.cancel);
        applyButton = (Button) findViewById(R.id.apply);
        clear = (Button) findViewById(R.id.clear);

        selectCountry = findViewById(R.id.selectCountry);
        selectCategory = findViewById(R.id.selectCategory);
        selectLanguage = findViewById(R.id.selectLanguage);

        List<Sources> categoryJson = new ArrayList<>();
        List<Sources> languageJson = new ArrayList<>();
        List<Sources> countryJson = new ArrayList<>();

        final SharedPreferences.Editor editor = getSharedPreferences("COUNTRY_SHARED_PREF", MODE_PRIVATE).edit();
        final ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.filter_options, country);
        selectCountry.setBackgroundColor(Color.WHITE);
        selectLanguage.setBackgroundColor(Color.LTGRAY);
        selectCategory.setBackgroundColor(Color.LTGRAY);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                String origName = countryOrig[position];
                editor.putString("country", origName);
                editor.putInt("countryId", (int) parent.getSelectedItemId());
                editor.apply();
                Toast.makeText(getApplicationContext(), (String) parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
            }
        });

        adapter.notifyDataSetChanged();

        SharedPreferences prefs1 = getSharedPreferences("COUNTRY_SHARED_PREF", MODE_PRIVATE);
        int selectedCateory = prefs1.getInt("categoryId", 0);
        String selectedCountry = prefs1.getString("country", null);
        String selectedLanguage = prefs1.getString("language", null);

        selectLanguage.setOnClickListener(this);
        selectCategory.setOnClickListener(this);
        selectCountry.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        applyButton.setOnClickListener(this);
        clear.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        final Intent intent = new Intent(SortingSources.this, Home.class);
        final SharedPreferences.Editor editor = getSharedPreferences("COUNTRY_SHARED_PREF", MODE_PRIVATE).edit();

        switch (v.getId()) {
            case R.id.selectCountry:
                final ArrayAdapter adapter1 = new ArrayAdapter<String>(this,
                        R.layout.filter_options, country);
                selectCountry.setBackgroundColor(Color.WHITE);
                selectLanguage.setBackgroundColor(Color.LTGRAY);
                selectCategory.setBackgroundColor(Color.LTGRAY);
                listView.setAdapter(adapter1);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        view.setSelected(true);
                        String origName = countryOrig[position];
                        editor.putString("country", origName);
                        editor.putInt("countryId", (int) parent.getSelectedItemId());
                        editor.apply();
                        Toast.makeText(getApplicationContext(), (String) parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                    }
                });

                adapter1.notifyDataSetChanged();
                break;


            case R.id.selectLanguage:
                ArrayAdapter adapter3 = new ArrayAdapter<String>(this,
                        R.layout.filter_options, language);
                selectLanguage.setBackgroundColor(Color.WHITE);
                selectCategory.setBackgroundColor(Color.LTGRAY);
                selectCountry.setBackgroundColor(Color.LTGRAY);
                listView.setAdapter(adapter3);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String origName = languageORIG[position];
                        editor.putString("language", origName);
                        editor.putInt("languageId", (int) parent.getSelectedItemId());
                        editor.apply();
                        view.setSelected(true);
                        Toast.makeText(getApplicationContext(), (String) parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                    }
                });
                adapter3.notifyDataSetChanged();
                break;


            case R.id.selectCategory:
                ArrayAdapter adapter2 = new ArrayAdapter<String>(this,
                        R.layout.filter_options, category);
                selectCategory.setBackgroundColor(Color.WHITE);
                selectLanguage.setBackgroundColor(Color.LTGRAY);
                selectCountry.setBackgroundColor(Color.LTGRAY);
                listView.setAdapter(adapter2);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                       // int name = (int) parent.getSelectedItemId();
                        String origName = categoryOrig[position];
                        editor.putString("category", origName);
                       // editor.putString("category", (String) parent.getItemAtPosition(position));
                        editor.putInt("categoryId", (int) parent.getSelectedItemId());
                        editor.apply();
                        view.setSelected(true);
                        Toast.makeText(getApplicationContext(), (String) parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                    }
                });
                adapter2.notifyDataSetChanged();
                break;

            case R.id.cancel:
                Intent intent1 = new Intent(SortingSources.this, Home.class);
                overridePendingTransition(0, R.anim.go_down);
                startActivity(intent1);
                finish();
                break;

            case R.id.apply:
                startActivity(intent);
                finish();
                break;

            case R.id.clear:
                editor.clear();
                editor.commit();
                listView.setSelected(false);
                Toast.makeText(getApplicationContext(), "Cleared Filters", Toast.LENGTH_SHORT).show();
                break;
        }
    }

//    @Override
//    public void onBackPressed() {
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Exit");
//            builder.setMessage(R.string.exit);
//
//            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    finish();
//                }
//            });
//
//            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                }
//            });
//            AlertDialog dialog = builder.show();
//
//
//    }
}
