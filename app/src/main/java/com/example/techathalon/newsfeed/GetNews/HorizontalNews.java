package com.example.techathalon.newsfeed.GetNews;


import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.techathalon.newsfeed.Adapters.NewsSourceAdapter;
import com.example.techathalon.newsfeed.Adapters.newsAdapter;
import com.example.techathalon.newsfeed.Adapters.sourceAdapter;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.example.techathalon.newsfeed.R;
import com.google.gson.JsonArray;
import com.takusemba.multisnaprecyclerview.MultiSnapRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HorizontalNews extends Fragment{

    MultiSnapRecyclerView multiSnapRecyclerView;
    RecyclerView recyclerView;

    NewsSourceAdapter adapter;

    JSONObject jsonObject, object;
    JSONArray jsonArray;

    private static final String API_KEY = "326b273af74c4a80913bca7789f2d898";
    String CATEGORY = "", COUNTRY = "", LANGUAGE = "";
    private ArrayList<Sources> newsSource;
    private ArrayList<String> newsLinks;

    // public String baseurl = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&language=" + LANGUAGE + "&country=" + COUNTRY + "&apiKey=" + API_KEY;
    public String baseurl = "https://newsapi.org/v2/sources?&apiKey=" + API_KEY;

    RequestQueue requestQueue;
    String imageUrl;
    SharedPrefConfig prefConfig;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_horizontal_news, container, false);
        newsSource = new ArrayList<>();
        newsLinks = new ArrayList<>();

        prefConfig = new SharedPrefConfig(getContext());

        multiSnapRecyclerView = rootview.findViewById(R.id.multi_recycler_view);

        imageUrl = prefConfig.getImageUrl();

       // adapter = new NewsSourceAdapter(getContext(), newsSource, HorizontalNews.this);
        adapter = new NewsSourceAdapter(getContext(), newsSource, imageUrl);

        JsonSorceResponse task = new JsonSorceResponse();
        task.execute();

        getFragmentManager().beginTransaction().replace(R.id.newFrame, new NewsHome()).commit();

        return rootview;
    }

    public class JsonSorceResponse extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String xml = "";

            if (COUNTRY == null && CATEGORY == null && LANGUAGE == null) {
                baseurl = "https://newsapi.org/v2/sources?category=&language=&country=&apiKey=" + API_KEY;
                xml = Connection.executeGet(baseurl);
            } else {
                baseurl = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&language=" + LANGUAGE + "&country=" + COUNTRY + "&apiKey=" + API_KEY;
                xml = Connection.executeGet(baseurl);
            }

            Log.d("link", baseurl);
            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {

            if (xml != null) //check if empty
            {
                try {
                    jsonObject = new JSONObject(xml);
                    jsonArray = jsonObject.getJSONArray("sources");

                    // databaseHelper.deleteJsonSource();

                    for (int i = 0; i < jsonArray.length(); i++) {

                        Sources mSource = new Sources();
                        object = jsonArray.getJSONObject(i);
                        mSource.setId(object.getString("id"));
                        mSource.setName(object.getString("name"));
                        mSource.setDescription(object.getString("description"));
                        mSource.setUrl(object.getString("url"));
                        mSource.setCategory(object.getString("category"));
                        mSource.setLanguage(object.getString("language"));
                        mSource.setCountry(object.getString("country"));
//                        Id = object.getString("id");
//                        Name = object.getString("name");
//                        Description = object.getString("description");
//                        Url = object.getString("url");
//                        Category = object.getString("category");
//                        Language = object.getString("language");
//                        Country = object.getString("country");
//                        databaseHelper.writeDatatoDb(Id, Name, Description, Url, Category, Language, Country

                        newsLinks.add(object.getString("url"));
                        newsSource.add(mSource);
                    }

                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                SnapHelper snapHelper = new PagerSnapHelper();

                multiSnapRecyclerView.setLayoutManager(linearLayoutManager);
//                snapHelper.attachToRecyclerView(multiSnapRecyclerView);
                multiSnapRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            } else {
                Toast.makeText(getContext(), "No news found", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
