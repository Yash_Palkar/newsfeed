package com.example.techathalon.newsfeed.GetNews;

import android.app.Service;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.constraint.solver.Cache;
import android.widget.Toast;

import com.bumptech.glide.load.engine.cache.DiskLruCacheFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Connection {
    int cacheSize = 10 * 1024 * 1024; // 10 MiB

    public static boolean isNetworkAvailable(Context context) {
        String status = "Not Connected to Internet";
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Service.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            boolean wificonnected = networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
            boolean mobileconnectec = networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
            if (wificonnected) {
                status = "You are connected to WiFi";
            } else if (mobileconnectec) {
                status = "You are connected to Mobile Internet";
            }
        }
//        Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    public static String executeGet(String targetUrl) {
        URL url;
        HttpURLConnection connection = null;


        try {
            url = new URL(targetUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("content-type", "application-json; charset=utf-8");
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(false);


            InputStream is;

            int status = connection.getResponseCode();

            if (status != HttpURLConnection.HTTP_OK) {
                is = connection.getErrorStream();
            } else {
                is = connection.getInputStream();
            }

            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {
            return null;

        } finally {

//            if (connection != null) {
                connection.disconnect();
//            }
        }

    }
}
