package com.example.techathalon.newsfeed.Database;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.techathalon.newsfeed.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;


public class Register extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    EditText name, email, pass, confpass, city, mobile;
    TextView login, detect, addimage;
    ImageView profilepic;
    Button reg;
    CheckBox chk;
    String lattitude, longitude, lattitude1, longitude1;
    DatabaseHelper databaseHelper;
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_GALLERY = 999;
    private static final int CAPTURE = 3;
    private static final int SELECT = 4;
    LocationManager locationManager;
    private ProgressDialog progressDialog;
    private int progressbarstatus = 0;
    private Handler progressbarhandler = new Handler();
    private boolean hasImageChanged = false;
    Bitmap thumbnail;
    Geocoder geocoder;
    List<Address> addresses;
    String addressLine, cityname, state, country, postalCode, knownName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootview = inflater.inflate(R.layout.fragment_register, container, false);
        databaseHelper = new DatabaseHelper(getContext());

        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        name = (EditText) rootview.findViewById(R.id.name);
        email = (EditText) rootview.findViewById(R.id.email);
        pass = (EditText) rootview.findViewById(R.id.password);
        confpass = (EditText) rootview.findViewById(R.id.confirm);
        login = (TextView) rootview.findViewById(R.id.alreadyuser);
        mobile = (EditText) rootview.findViewById(R.id.mobile);
        chk = (CheckBox) rootview.findViewById(R.id.check);
        reg = (Button) rootview.findViewById(R.id.regbutton);
        city = rootview.findViewById(R.id.location);
        detect = rootview.findViewById(R.id.detect);
        addimage = (TextView) rootview.findViewById(R.id.addimage);
        profilepic = (ImageView) rootview.findViewById(R.id.profilepic);

        //lattitude and longitude


        detect.setOnClickListener(this);
        reg.setOnClickListener(this);
        login.setOnClickListener(this);
        chk.setOnCheckedChangeListener(this);
        city.setOnClickListener(this);
        addimage.setOnClickListener(this);


        //Uploading image
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            profilepic.setEnabled(false);
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            profilepic.setEnabled(true);
        }

        return rootview;
    }



    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 10 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                mobile.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public static boolean isValidPassword(String password) {
        Matcher matcher = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{4,20})").matcher(password);
        return matcher.matches();
    }

    public void RegisterData() {
        if (name.getText().toString().equals("") || email.getText().toString().equals("") || mobile.getText().toString().equals("") || pass.getText().toString().equals("") || confpass.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Fields are empty", Toast.LENGTH_SHORT).show();
        } else {
            String e1 = name.getText().toString();
            String e2 = email.getText().toString();
            String e3 = pass.getText().toString();
            String e4 = confpass.getText().toString();
            String e5 = mobile.getText().toString();
            String e6 = lattitude1;
            String e7 = longitude1;

            profilepic.setDrawingCacheEnabled(true);
            profilepic.buildDrawingCache();
            Bitmap bitmap = profilepic.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] e8 = baos.toByteArray();

            if (isValidEmail(e2)) {

                if (isValidMobile(e5)) {

                    if (isValidPassword(e3)) {

                        if (e3.equals(e4)) {
                            boolean check = databaseHelper.checkAlreadyExist(e2);
                            if (check == true) {
                                boolean in = databaseHelper.insert(e1, e2, e3, e5, e6, e7, e8);
                                if (in == true) {
                                    Toast.makeText(getContext(), "Registered Successfully", Toast.LENGTH_SHORT).show();
                                    name.setText("");
                                    email.setText("");
                                    pass.setText("");
                                    confpass.setText("");
                                    mobile.setText("");
                                    city.setText("");
                                    profilepic.setImageResource(R.drawable.profilepic);
                                } else {
                                    Toast.makeText(getContext(), "Some Error Occured", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "Email Alreay Exist", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Password Does Not Match", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Password must contain mix of upper and lower case letters as well as digits and one special charecter(4-20)", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Please Enter Valid Contact Number", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), "Please Enter Valid Email Address", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            confpass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            confpass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);


            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                geocoder = new Geocoder(getContext(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    addressLine = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    cityname = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                city.setText(cityname + ", " + state + ", " + country);

            } else if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);


                geocoder = new Geocoder(getContext(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    addressLine = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    cityname = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                city.setText(cityname + ", " + state + ", " + country);


            } else if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                geocoder = new Geocoder(getContext(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    addressLine = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    cityname = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                city.setText(cityname + ", " + state + ", " + country);

            } else {

                Toast.makeText(getContext(), "Unable to Trace your location", Toast.LENGTH_SHORT).show();

            }
        }

        lattitude1 = lattitude;
        longitude1 = longitude;
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public void addImage() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.uploadImages)
                .items(R.array.uploadImages)
                .itemsIds(R.array.itemIds)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                photoPickerIntent.setType("image/*");
                                startActivityForResult(photoPickerIntent, SELECT);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, CAPTURE);
                                break;
                            case 2:
                                profilepic.setImageResource(R.drawable.profilepic);
                                break;
                        }
                    }
                })
                .show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    profilepic.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCapturedImageResult(data);
            }
        }
    }

    private void onCapturedImageResult(Intent data) {

        thumbnail = (Bitmap) data.getExtras().get("data");

        profilepic.setImageBitmap(thumbnail);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regbutton:
                RegisterData();
                break;

            case R.id.alreadyuser:
                ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
                viewPager.setCurrentItem(0);
                //getFragmentManager().beginTransaction().replace(R.id.fragmentcontainer, new Login()).commit();
                break;

            case R.id.detect:
                Toast.makeText(getActivity(), "Detect Location", Toast.LENGTH_SHORT).show();
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();

                } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    getLocation();
                }
                break;

            case R.id.addimage:
                addImage();
                break;
        }
    }

}
