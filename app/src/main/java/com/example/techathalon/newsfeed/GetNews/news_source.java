package com.example.techathalon.newsfeed.GetNews;


import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.techathalon.newsfeed.Adapters.newsAdapter;
import com.example.techathalon.newsfeed.Adapters.sourceAdapter;
import com.example.techathalon.newsfeed.Database.DatabaseHelper;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.example.techathalon.newsfeed.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class news_source extends Fragment implements View.OnClickListener {

    String filterUrlPattern = "https://newsapi.org/v2/sources?category=general&language=en&country=us&apiKey=326b273af74c4a80913bca7789f2d898";

    private static final String API_KEY = "326b273af74c4a80913bca7789f2d898";
    String CATEGORY = "", COUNTRY = "", LANGUAGE = "";
    JSONObject jsonObject, object;
    JSONArray jsonArray;
    //  public static String baseurl = "https://newsapi.org/v2/sources?&apiKey=" + API_KEY;

    public String baseurl = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&language=" + LANGUAGE + "&country=" + COUNTRY + "&apiKey=" + API_KEY;


    public String anotherulr = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&apiKey=" + API_KEY;
    private RecyclerView recyclerView;
    private ImageView emptyView;
    private ArrayList<Sources> newsSource;
    private Set<String> spinnerCountry;
    private Set<String> spinnerCategory;
    ProgressDialog progressDialog;
    int noOfColumns;
    Toolbar toolbar;
    SwipeRefreshLayout swipeRefreshLayout;
    SearchView searchView;
    sourceAdapter mAdapter;
    Button theme, filter;
    DatabaseHelper databaseHelper;
    RequestQueue requestQueue;
    StringRequest request;
    ConstraintLayout constraintLayout;
    SharedPrefConfig sharedPrefConfig;

    String Id = "", Name = "", Description = "", Url = "", Category = "", Language = "", Country = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_news_source, container, false);

        databaseHelper = new DatabaseHelper(getContext());

        sharedPrefConfig = new SharedPrefConfig(getContext());

//        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//        getActivity().setTheme(R.style.NightModeTheme);
//        // Activate Night Mode
//        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
//           getActivity().setTheme(R.style.NightModeTheme);
//        }else {
//            getActivity().setTheme(R.style.AppTheme);
//        }

        SharedPreferences prefs1 = getActivity().getSharedPreferences("COUNTRY_SHARED_PREF", MODE_PRIVATE);
        String selectedCateory = prefs1.getString("category", null);
        String selectedCountry = prefs1.getString("country", null);
        String selectedLanguage = prefs1.getString("language", null);
        if (selectedCountry != null) {
            COUNTRY = prefs1.getString("country", selectedCountry);
        } else {
            COUNTRY = "";
        }
        if (selectedCateory != null) {
            CATEGORY = prefs1.getString("category", selectedCateory);//"No name defined" is the default value.
        } else {
            CATEGORY = "";
        }

        if (selectedLanguage != null) {
            LANGUAGE = prefs1.getString("language", selectedLanguage);
        } else {
            LANGUAGE = "";
        }

        Log.d("CATEGORY", CATEGORY + " It is selected item");

        recyclerView = (RecyclerView) rootview.findViewById(R.id.newssourcerecycler);
        swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.newsSwipeRefresh);
        emptyView = (ImageView) rootview.findViewById(R.id.no_data);
        constraintLayout = (ConstraintLayout) rootview.findViewById(R.id.constraint_view);

        mAdapter = new sourceAdapter(getContext(), newsSource, this);

//        theme = (Button) rootview.findViewById(R.id.themeButton);
//        theme.setOnClickListener(this);

        filter = (Button) rootview.findViewById(R.id.filterButton);
        filter.setOnClickListener(this);

        newsSource = new ArrayList<>();

        if (Connection.isNetworkAvailable(getContext())) {
            progressDialog = new ProgressDialog(getContext());
            emptyView.setVisibility(View.GONE);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            NewsResponse();

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    newsSource.clear();
                    emptyView.setVisibility(View.GONE);
                    NewsResponse();
                    swipeRefreshLayout.setRefreshing(false);
                    mAdapter.notifyDataSetChanged();
                }
            });

        } else {
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Toast.makeText(getContext(), "Please Connect To Internet", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
            // readFromInternalStorage();
            Cursor cursor = databaseHelper.getJsonData();

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Sources mSource = new Sources();
                    Id = cursor.getString(0);
                    Name = cursor.getString(1);
                    Description = cursor.getString(2);
                    Url = cursor.getString(3);
                    Category = cursor.getString(4);
                    Language = cursor.getString(5);
                    Country = cursor.getString(6);

                    mSource.setId(Id);
                    mSource.setName(Name);
                    mSource.setDescription(Description);
                    mSource.setUrl(Url);
                    mSource.setCategory(Category);
                    mSource.setLanguage(Language);
                    mSource.setCountry(Country);
                    newsSource.add(mSource);
                }
                while (cursor.moveToNext());

                Snackbar.make(container, "No Internet", Snackbar.LENGTH_LONG).show();

                sourceAdapter adapter = new sourceAdapter(getActivity(), newsSource, news_source.this);
                int mNoOfColumns = calculateNoOfColumns(getActivity());

                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                recyclerView.setAdapter(adapter);
                recyclerView.getRecycledViewPool().clear();
                adapter.notifyDataSetChanged();

            }

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (Connection.isNetworkAvailable(getContext())) {
                        newsSource.clear();
                        progressDialog = new ProgressDialog(getContext());
                        emptyView.setVisibility(View.GONE);
                        progressDialog.setMessage("Loading...");
                        progressDialog.show();
                        emptyView.setVisibility(View.GONE);
                        NewsResponse();
                        swipeRefreshLayout.setRefreshing(false);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getContext(), "Please Connect To Internet", Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            });
        }
        emptyView.setVisibility(View.GONE);
        return rootview;
    }


    private void NewsResponse() {
        baseurl = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&language=" + LANGUAGE + "&country=" + COUNTRY + "&apiKey=" + API_KEY;

        requestQueue = Volley.newRequestQueue(getContext());


            request = new StringRequest(baseurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        newsSource = new ArrayList<>();
                        jsonObject = new JSONObject(response);
                        jsonArray = jsonObject.getJSONArray("sources");


//                    if (!sharedPrefConfig.readDataOffline()){
                        databaseHelper.deleteJsonSource();
//                    }

                        if (jsonArray == null) {
                            recyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        } else {
                            emptyView.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                        for (int i = 0; i < jsonArray.length(); i++) {

                            Sources mSource = new Sources();
                            object = jsonArray.getJSONObject(i);
                            mSource.setId(object.getString("id"));
                            mSource.setName(object.getString("name"));
                            mSource.setDescription(object.getString("description"));
                            mSource.setUrl(object.getString("url"));
                            mSource.setCategory(object.getString("category"));
                            mSource.setLanguage(object.getString("language"));
                            mSource.setCountry(object.getString("country"));

                            Id = object.getString("id");
                            Name = object.getString("name");
                            Description = object.getString("description");
                            Url = object.getString("url");
                            Category = object.getString("category");
                            Language = object.getString("language");
                            Country = object.getString("country");

                            databaseHelper.writeDatatoDb(Id, Name, Description, Url, Category, Language, Country);
                            newsSource.add(mSource);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
                    }

                    if (newsSource.isEmpty()) {
                        recyclerView.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        emptyView.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                    sourceAdapter adapter = new sourceAdapter(getActivity(), newsSource, news_source.this);
                    //   int mNoOfColumns = calculateNoOfColumns(getActivity());
                    recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    progressDialog.cancel();
                    //saveToInternalStorage();

                    adapter.notifyDataSetChanged();

                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Volley", error.toString());
                }
            });
        if (request == null) {
            Toast.makeText(getContext(), "API hit 1000 times", Toast.LENGTH_SHORT).show();
            progressDialog.cancel();
        } else {
            requestQueue.add(request);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.themeButton:
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
////                if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
////                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
////                }else {
////                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
////                }
//                break;

            case R.id.filterButton:
                Intent intent = new Intent(getActivity(), SortingSources.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.go_up, 0);
                //getActivity().finish();
                break;
        }
    }

    public int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        noOfColumns = (int) (dpWidth / 150);
        return noOfColumns;
    }

    public void saveToInternalStorage() {
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File path = new File(getContext().getFilesDir() + "/NewsData");
            File myDir = new File(root + "/NewsData");
            myDir.mkdirs();
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "NewsSource.txt";
            File file = new File(myDir, fname);
            if (file.exists()) file.delete();
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream of = new ObjectOutputStream(fos);
            of.writeObject(newsSource);
            of.flush();
            of.close();
            fos.close();
        } catch (Exception e) {
            Log.e("InternalStorage", e.getMessage());
        }
    }

    public ArrayList<Sources> readFromInternalStorage() {
        ArrayList<Sources> toReturn = null;
        FileInputStream fis;
        try {
            fis = getContext().openFileInput("NewsSource.txt");
            ObjectInputStream oi = new ObjectInputStream(fis);
            toReturn = (ArrayList<Sources>) oi.readObject();
            oi.close();
            sourceAdapter adapter = new sourceAdapter(getActivity(), toReturn, news_source.this);

            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), noOfColumns));
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
//            progressDialog.cancel();
        } catch (FileNotFoundException e) {
            Log.e("InternalStorage", e.getMessage());
        } catch (IOException e) {
            Log.e("InternalStorage", e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return toReturn;
    }

}
