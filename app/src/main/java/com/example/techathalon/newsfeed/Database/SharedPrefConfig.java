package com.example.techathalon.newsfeed.Database;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.techathalon.newsfeed.R;

import java.util.HashSet;
import java.util.Set;

public class SharedPrefConfig {

    private SharedPreferences sharedPreferences;
    private Context context;

    public SharedPrefConfig(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.login_preference), Context.MODE_PRIVATE);
    }

    public void writeLoginStatus(boolean status) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.login_status_preference), status);
        editor.commit();
    }

    public void clear() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public boolean readLoginStatus() {
        boolean status = false;
        status = sharedPreferences.getBoolean(context.getResources().getString(R.string.login_status_preference), false);
        return status;
    }

    public void writeGoogleLoginStatus(boolean status) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().getString(R.string.google_login_status_preference), status);
        editor.commit();

    }

    public boolean readGoogleLoginStatus() {
        boolean status = false;
        status = sharedPreferences.getBoolean(context.getResources().getString(R.string.google_login_status_preference), false);
        return status;
    }


    public void lattitude(String latt) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Lattitude", latt);
        editor.commit();

    }

    public String getlatt() {
        String latt = sharedPreferences.getString("Lattitude", "");
        return latt;
    }


    public void loggedInUserEmail(String email) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("UserEmail", email);
        editor.commit();
    }


    public String getLoggedInUser() {
        String loggedInEmail = sharedPreferences.getString("UserEmail", "");
        return loggedInEmail;
    }

    public void longitude(String longi) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Longitude", longi);
        editor.commit();
    }

    public String getLong() {
        String longi = sharedPreferences.getString("Longitude", "");
        return longi;
    }

    public String getName() {
        String name = sharedPreferences.getString("LoginAs", "");
        return name;
    }

    public void newsImageUrl(String imageUrl) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("news_image", imageUrl);
        editor.commit();
    }

    public String getImageUrl() {
        String imageUrl = sharedPreferences.getString("news_image", "");
        return imageUrl;
    }

    public boolean readDataOffline() {
        boolean save = false;
        save = sharedPreferences.getBoolean(context.getResources().getString(R.string.save_offline_preference), false);
        return save;

    }

    public void saveDataOffline(boolean save) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(context.getResources().
                getString(R.string.save_offline_preference), save);
        editor.commit();
    }


}
