package com.example.techathalon.newsfeed.Database;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.techathalon.newsfeed.Database.DatabaseHelper;
import com.example.techathalon.newsfeed.Home;
import com.example.techathalon.newsfeed.MainActivity;
import com.example.techathalon.newsfeed.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConfirmPassword extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    EditText prevpass, pass, pass1;
    Button btn;
    CheckBox chk;
    DatabaseHelper databaseHelper;
    SharedPrefConfig prefConfig;
    String email, userpass, useremail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_password);

        prevpass = (EditText) findViewById(R.id.prevpass);
        pass = (EditText) findViewById(R.id.resetpass);
        pass1 = (EditText) findViewById(R.id.confirmreset);
        btn = (Button) findViewById(R.id.resetButton);
        chk = (CheckBox) findViewById(R.id.check);
        chk.setOnCheckedChangeListener(this);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        prefConfig = new SharedPrefConfig(getApplicationContext());
        useremail = prefConfig.getLoggedInUser();

        Cursor cursor = databaseHelper.getUserData(useremail);
        if (cursor != null) {
            do {

                userpass = cursor.getString(3);

            } while (cursor.moveToNext());

        }
        btn.setOnClickListener(this);
    }

    public static boolean isValidPassword(String password) {
        Matcher matcher = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{4,20})").matcher(password);
        return matcher.matches();
    }

    @Override
    public void onClick(View v) {
        String p1 = pass.getText().toString();
        String p2 = pass1.getText().toString();
        String p3 = prevpass.getText().toString();

        if (isValidPassword(p1)) {
            if (p3.equals(userpass)) {
                if (p1.equals(p2)) {
                    boolean rs = databaseHelper.resetPasswrod(useremail, p1);
                    if (rs) {
                        Toast.makeText(getApplicationContext(), "Password Set Successfully", Toast.LENGTH_SHORT).show();
                        prevpass.setText("");
                        pass.setText("");
                        pass1.setText("");
                        Intent intent = new Intent(this, Home.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Password Does Not Match", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "You entered wrong password", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Password must contain mix of upper and lower case letters as well as digits and one special charecter(4-20)", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            pass1.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            pass1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }
}
