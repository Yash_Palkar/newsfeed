package com.example.techathalon.newsfeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.techathalon.newsfeed.Database.DatabaseHelper;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {

    DatabaseHelper databaseHelper;
    private SharedPrefConfig prefConfig;
    Button logout;
    double latt = 0.0, longi = 0.0;
    private GoogleMap mMap;
    ActionBar ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

       ab = getSupportActionBar();
       ab.setDisplayShowHomeEnabled(true);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        databaseHelper = new DatabaseHelper(this);
        prefConfig = new SharedPrefConfig(getApplicationContext());

        logout = (Button) findViewById(R.id.logout);
        logout.setOnClickListener(this);


        if (!prefConfig.readLoginStatus()) {
            Intent intent = new Intent(this, Home.class);
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
            finish();
        }

        String val1 = prefConfig.getlatt();
        String val2 = prefConfig.getLong();

        if(val1==null && val2 == null){
            latt = 0.0;
            longi = 0.0;
        }else{
            latt = Double.parseDouble(val1);
            longi = Double.parseDouble(val2);
        }
     }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //  zoom levels
//        1: World
//        5: Landmass/continent
//        10: City
//        15: Streets
//        20: Buildings

        // Add a marker in Sydney and move the camera

        if(latt==0.0 || longi == 0.0){
            LatLng defaultlocation = new LatLng(28.739260,  77.075611);
            mMap.addMarker(new MarkerOptions().position(defaultlocation).title("Default Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultlocation));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));

        }else{
            LatLng mylocation = new LatLng(latt, longi);
            mMap.addMarker(new MarkerOptions().position(mylocation).title("My Location is Here"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(mylocation));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
        }


    }

    public void logout() {
        prefConfig.writeLoginStatus(false);
        //  preferenceConfig.clearPreference();
        Toast.makeText(this, "Logged Out", Toast.LENGTH_SHORT).show();
        Intent logoutIntent = new Intent(this, MainActivity.class);
        startActivity(logoutIntent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout:
                logout();
                break;
        }
    }
}
