package com.example.techathalon.newsfeed.GetNews;


import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.techathalon.newsfeed.Adapters.FinalAdapter;
import com.example.techathalon.newsfeed.Adapters.NewsSourceAdapter;
import com.example.techathalon.newsfeed.Adapters.newsAdapter;
import com.example.techathalon.newsfeed.Adapters.sourceAdapter;
import com.example.techathalon.newsfeed.Database.DatabaseHelper;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.example.techathalon.newsfeed.R;
import com.takusemba.multisnaprecyclerview.MultiSnapRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.IDN;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewNews extends Fragment {

    ViewPager pager;
    FinalAdapter adapter;
    MultiSnapRecyclerView multiSnapRecyclerView;
    RecyclerView recyclerView;
    TabLayout tab;
    ProgressDialog progressDialog;
    Toolbar toolbar;
    JSONObject jsonObject, object;
    DatabaseHelper databaseHelper;
    JSONArray jsonArray;
    ImageButton leftNav, rightNav;

    private static final String API_KEY = "326b273af74c4a80913bca7789f2d898";
    String CATEGORY = "", COUNTRY = "", LANGUAGE = "";
    private ArrayList<Sources> newsSource;
    private ArrayList<String> newsLinks;
    private ArrayList<String> newsTitle;

    SwipeRefreshLayout swipeRefreshLayout;

    String Id = "", Name = "", Description = "", Url = "", Category = "", Language = "", Country = "";

    // public String baseurl = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&language=" + LANGUAGE + "&country=" + COUNTRY + "&apiKey=" + API_KEY;
    public String baseurl = "https://newsapi.org/v2/sources?&apiKey=" + API_KEY;

    RequestQueue requestQueue;
    StringRequest request;
    String imageUrl;
    SharedPrefConfig prefConfig;
    int pagePosition=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_view_news, container, false);
        databaseHelper = new DatabaseHelper(getContext());

        pager = (ViewPager) rootview.findViewById(R.id.news_pager);
        tab = (TabLayout) rootview.findViewById(R.id.recycler_tab);
        toolbar = (Toolbar) rootview.findViewById(R.id.tab_toolbar);
        swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipe_refresh_layout);
        leftNav = (ImageButton) rootview.findViewById(R.id.left_nav);
        rightNav = (ImageButton) rootview.findViewById(R.id.right_nav);
       pager.setOffscreenPageLimit(2);

        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = pager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    pager.setCurrentItem(tab);
                } else if (tab == 0) {
                    pager.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = pager.getCurrentItem();
                tab++;
                pager.setCurrentItem(tab);
            }
        });

        newsSource = new ArrayList<>();
        newsLinks = new ArrayList<>();
        newsTitle = new ArrayList<>();

        prefConfig = new SharedPrefConfig(getContext());

        imageUrl = prefConfig.getImageUrl();

        if (Connection.isNetworkAvailable(getContext())) {
            progressDialog = new ProgressDialog(getContext());
            //  emptyView.setVisibility(View.GONE);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            NewsResponse();

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    newsSource.clear();
                    NewsResponse();
                    swipeRefreshLayout.setRefreshing(false);
                    adapter.notifyDataSetChanged();
                }
            });
        } else {

            Snackbar.make(container, "No Internet Connection", Snackbar.LENGTH_INDEFINITE).show();
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Toast.makeText(getContext(), "Please Connect To Internet", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
            // readFromInternalStorage();
            Cursor cursor = databaseHelper.getJsonData();

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Sources mSource = new Sources();
                    Id = cursor.getString(0);
                    Name = cursor.getString(1);
                    Description = cursor.getString(2);
                    Url = cursor.getString(3);
                    Category = cursor.getString(4);
                    Language = cursor.getString(5);
                    Country = cursor.getString(6);

                    mSource.setId(Id);
                    mSource.setName(Name);
                    mSource.setDescription(Description);
                    mSource.setUrl(Url);
                    mSource.setCategory(Category);
                    mSource.setLanguage(Language);
                    mSource.setCountry(Country);

                    newsLinks.add(Id);
                    newsTitle.add(Name);
                    newsSource.add(mSource);
                }
                while (cursor.moveToNext());
                adapter = new FinalAdapter(getChildFragmentManager(), newsLinks, newsTitle);
                pager.setAdapter(adapter);
              //  pager.setCurrentItem(pagePosition, true);
                tab.setupWithViewPager(pager);
                adapter.notifyDataSetChanged();
            }

        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Connection.isNetworkAvailable(getContext())) {

                    newsSource.clear();
                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                    NewsResponse();
                    progressDialog.cancel();

                    swipeRefreshLayout.setRefreshing(false);
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Please Connect To Internet", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
//
        return rootview;
    }


    private void NewsResponse() {
        baseurl = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&language=" + LANGUAGE + "&country=" + COUNTRY + "&apiKey=" + API_KEY;

        requestQueue = Volley.newRequestQueue(getContext());

        request = new StringRequest(baseurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    newsSource = new ArrayList<>();
                    jsonObject = new JSONObject(response);
                    jsonArray = jsonObject.getJSONArray("sources");

//                    if (!prefConfig.readDataOffline()){
//                        databaseHelper.deleteJsonSource();
//                    }

                    databaseHelper.deleteJsonSource();

                    for (int i = 0; i < jsonArray.length(); i++) {

                        Sources mSource = new Sources();
                        object = jsonArray.getJSONObject(i);
                        mSource.setId(object.getString("id"));
                        mSource.setName(object.getString("name"));
                        mSource.setDescription(object.getString("description"));
                        mSource.setUrl(object.getString("url"));
                        mSource.setCategory(object.getString("category"));
                        mSource.setLanguage(object.getString("language"));
                        mSource.setCountry(object.getString("country"));

                        Id = object.getString("id");
                        Name = object.getString("name");
                        Description = object.getString("description");
                        Url = object.getString("url");
                        Category = object.getString("category");
                        Language = object.getString("language");
                        Country = object.getString("country");

                        databaseHelper.writeDatatoDb(Id, Name, Description, Url, Category, Language, Country);

                        newsLinks.add(object.getString("id"));
                        newsTitle.add(object.getString("name"));
                       // pagePosition = jsonArray.getInt(i);
                        newsSource.add(mSource);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
                }

//                if (newsSource.isEmpty()) {
//                    recyclerView.setVisibility(View.GONE);
//                  //  emptyView.setVisibility(View.VISIBLE);
//                } else {
//                //    emptyView.setVisibility(View.GONE);
//                    recyclerView.setVisibility(View.VISIBLE);
//                }


                adapter = new FinalAdapter(getChildFragmentManager(), newsLinks, newsTitle);
                pager.setAdapter(adapter);
                tab.setupWithViewPager(pager);
                progressDialog.cancel();
                adapter.notifyDataSetChanged();


            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
            }
        });

        requestQueue.add(request);

    }

    public class NewsResponseAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            String xml = "";

            if (COUNTRY == null && CATEGORY == null && LANGUAGE == null) {
                baseurl = "https://newsapi.org/v2/sources?category=&language=&country=&apiKey=" + API_KEY;
                xml = Connection.executeGet(baseurl);
            } else {
                baseurl = "https://newsapi.org/v2/sources?category=" + CATEGORY + "&language=" + LANGUAGE + "&country=" + COUNTRY + "&apiKey=" + API_KEY;
                xml = Connection.executeGet(baseurl);
            }
            Log.d("link", baseurl);
            return xml;
        }

        @Override
        protected void onPostExecute(String xml) {

            if (xml != null) //check if empty
            {
                try {
                    jsonObject = new JSONObject(xml);
                    jsonArray = jsonObject.getJSONArray("sources");

                    databaseHelper.deleteJsonSource();

                    for (int i = 0; i < jsonArray.length(); i++) {

                        Sources mSource = new Sources();
                        object = jsonArray.getJSONObject(i);
                        mSource.setId(object.getString("id"));
                        mSource.setName(object.getString("name"));
                        mSource.setDescription(object.getString("description"));
                        mSource.setUrl(object.getString("url"));
                        mSource.setCategory(object.getString("category"));
                        mSource.setLanguage(object.getString("language"));
                        mSource.setCountry(object.getString("country"));

                        Id = object.getString("id");
                        Name = object.getString("name");
                        Description = object.getString("description");
                        Url = object.getString("url");
                        Category = object.getString("category");
                        Language = object.getString("language");
                        Country = object.getString("country");
                        databaseHelper.writeDatatoDb(Id, Name, Description, Url, Category, Language, Country);

                        newsLinks.add(object.getString("id"));
                        newsTitle.add(object.getString("name"));

                        newsSource.add(mSource);
                    }

                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
                }

                adapter = new FinalAdapter(getChildFragmentManager(), newsLinks, newsTitle);
                pager.setAdapter(adapter);
                tab.setupWithViewPager(pager);
                adapter.notifyDataSetChanged();
                progressDialog.cancel();

            } else {
                Toast.makeText(getContext(), "No news found", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
