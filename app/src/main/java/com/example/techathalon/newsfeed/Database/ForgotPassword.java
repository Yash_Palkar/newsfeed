package com.example.techathalon.newsfeed.Database;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.techathalon.newsfeed.R;


public class ForgotPassword extends Fragment implements View.OnClickListener {
    private static final String StringUtils = "";


    //created with professor DK channel utube

    EditText email;
    DatabaseHelper databaseHelper;
    Button reset, sendmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        email = (EditText) rootview.findViewById(R.id.resetemail);
        databaseHelper = new DatabaseHelper(getContext());

        sendmail = (Button) rootview.findViewById(R.id.sendmail);
        sendmail.setOnClickListener(this);

        return rootview;
    }



    public void sendEMail(){

        if(email.getText().toString().equals("")){
            Toast.makeText(getContext(), "Please Enter Email ID", Toast.LENGTH_SHORT).show();
            return;
        }
        databaseHelper.getName(email.getText().toString());
        String to = email.getText().toString();
        String subject = "Reset Your NewsFeed Password";
        String msg = "To reset your password click here";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mail to:"));
        intent.putExtra(Intent.EXTRA_EMAIL, to);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, msg);
        intent.setType("message/rfc822");
        startActivity(intent); email.setText("");

        Toast.makeText(getContext(), "Email Sent", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){


            case R.id.sendmail:
                sendEMail();
                break;
        }

    }

}
