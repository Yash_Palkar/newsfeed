package com.example.techathalon.newsfeed.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.techathalon.newsfeed.GetNews.DetailsWebView;
import com.example.techathalon.newsfeed.GetNews.NewsHome;
import com.example.techathalon.newsfeed.GetNews.Sources;
import com.example.techathalon.newsfeed.GetNews.news_source;
import com.example.techathalon.newsfeed.R;
import com.github.florent37.diagonallayout.DiagonalLayout;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class sourceAdapter extends RecyclerView.Adapter<sourceAdapter.SourceHolder> {
    private Context context;
    private ArrayList<Sources> mData;
    private Fragment fragment;

    public sourceAdapter(Context context, ArrayList<Sources> mData, Fragment fragment) {
        this.context = context;
        this.mData = mData;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public SourceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.sorces_layout, viewGroup, false);

        return new SourceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SourceHolder holder, final int position) {
        holder.title.setText(mData.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsHome newFragment = new NewsHome();
                Bundle bundle = new Bundle();
                bundle.putString("link", mData.get(position).getId());
                newFragment.setArguments(bundle);
                ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.framelayout, newFragment).addToBackStack("news_source").commit();

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public class SourceHolder extends RecyclerView.ViewHolder {
        TextView title;

        public SourceHolder(@NonNull final View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }


    public void updateList(List<Sources> newList) {
        mData = new ArrayList<>();
        mData.addAll(newList);
        notifyDataSetChanged();
    }
}















