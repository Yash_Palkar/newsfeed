package com.example.techathalon.newsfeed.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.techathalon.newsfeed.GetNews.DetailsWebView;
import com.example.techathalon.newsfeed.GetNews.HorizontalNews;
import com.example.techathalon.newsfeed.GetNews.ISO8601Parse;
import com.example.techathalon.newsfeed.GetNews.NewsHome;
import com.example.techathalon.newsfeed.GetNews.news;
import com.example.techathalon.newsfeed.R;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.github.florent37.diagonallayout.DiagonalLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static java.security.AccessController.getContext;

public class newsAdapter extends RecyclerView.Adapter<newsAdapter.newsHolder> implements Filterable {

    private Context context;
    private ArrayList<news> mData;

    //this list is to create full list after clearing the search view
    ArrayList<news> tempData;

    public newsAdapter(Context context, ArrayList<news> newsList) {
        this.context = context;
        this.mData = newsList;
    }

    @NonNull
    @Override
    public newsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.news_layout, viewGroup, false);
        return new newsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final newsHolder holder, final int position) {

        Date date = null;

        try {
            date = ISO8601Parse.parse(mData.get(position).getPublishAt());
            holder.date.setReferenceTime(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.title.setText(mData.get(position).getTitle());
        holder.desc.setText(mData.get(position).getDescription());
        Glide.with(context).load(mData.get(position).getImage_url()).into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsWebView.class);
                intent.putExtra("url", mData.get(position).getUrl());
                v.getContext().startActivity(intent);

            }
        });
    }


    public void removeItem(int position) {
        mData.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(news item, int position) {
        mData.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private Filter fRecords;


    @Override
    public Filter getFilter() {
        if (fRecords == null) {
            fRecords = new RecordFilter();
        }
        return fRecords;
    }

    private class RecordFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {


            String filterString = constraint.toString().toLowerCase();
            FilterResults oReturn = new FilterResults();
            int count = mData.size();
            ArrayList<news> results = new ArrayList<>();

            if (tempData == null) {
                tempData = mData;
            }

            if (constraint != null) {
                if (tempData != null && tempData.size() > 0) {
                    for (final news g : tempData) {
                        if (g.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            results.add(g);
                        }
                    }
                }
                oReturn.values = results;
            }
            return oReturn;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // mData.clear();
            mData = (ArrayList<news>) results.values;
            notifyDataSetChanged();
        }
    }

    public class newsHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title, desc;
        RelativeTimeTextView date;
        public LinearLayout background;
        public ConstraintLayout foreground;

        public newsHolder(@NonNull final View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.newsimage);
            title = (TextView) itemView.findViewById(R.id.newstitle);
            desc = (TextView) itemView.findViewById(R.id.newsdesc);
            date = (RelativeTimeTextView) itemView.findViewById(R.id.newsdate);
            background = (LinearLayout) itemView.findViewById(R.id.swipe_layout);
            foreground = (ConstraintLayout) itemView.findViewById(R.id.constraint);
          //  delete =(ImageView) itemView.findViewById(R.id.delete_news);
        }
    }
}

