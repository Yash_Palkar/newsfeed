package com.example.techathalon.newsfeed.GetNews;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Call;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.techathalon.newsfeed.R;
import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DetailsWebView extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton sharebutton, fbButton, googleButton, waButton;
    ProgressBar loader;
    WebView webView;
    ImageView offline;
    String url = "";
    boolean isOpen = false;
    Animation fabOpen, fabClose;
    CallbackManager callbackManager;
    ShareDialog shareDialog;
    Button refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_web_view);

        sharebutton = (FloatingActionButton) findViewById(R.id.share);
        fbButton = (FloatingActionButton) findViewById(R.id.facebook);
        googleButton = (FloatingActionButton) findViewById(R.id.google);
        waButton = (FloatingActionButton) findViewById(R.id.whatsapp);
        refresh = (Button) findViewById(R.id.refresh_button);
        offline = (ImageView) findViewById(R.id.offline);
        offline.setVisibility(View.GONE);
        fbButton.setClickable(false);
        googleButton.setClickable(false);
        waButton.setClickable(false);

        fabOpen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);

        fbButton.startAnimation(fabClose);
        googleButton.startAnimation(fabClose);
        waButton.startAnimation(fabClose);

        loader = (ProgressBar) findViewById(R.id.loader);
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        Log.d("sonali", url);
        webView = (WebView) findViewById(R.id.webview);

        if (!Connection.isNetworkAvailable(this)) {
            offline.setVisibility(View.VISIBLE);
        }

        //share on fb
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);


        webView.getSettings().setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT); // load online by default

        if (!Connection.isNetworkAvailable(getApplicationContext())) { // loading offline
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }

        webView.getSettings().setJavaScriptEnabled(true);
        if (getIntent() != null) {
            if (!getIntent().getStringExtra("url").isEmpty()) {
                webView.loadUrl(getIntent().getStringExtra("url"));
            }else {
               // webView.loadUrl(getIntent().getStringExtra("url"));
                offline.setVisibility(View.VISIBLE);
                sharebutton.setClickable(false);

                webView.setVisibility(View.GONE);
            }
        }

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                loader.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loader.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                //  webView.loadUrl(getIntent().getStringExtra("url"));
                // offline.setVisibility(View.VISIBLE);
                // sharebutton.setClickable(false);
                //  view.setBackgroundColor(Color.parseColor("#f7f7f7"));
                //webView.setVisibility(View.GONE);
            }
        });


        sharebutton.setOnClickListener(this);
        fbButton.setOnClickListener(this);
        googleButton.setOnClickListener(this);
        waButton.setOnClickListener(this);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.loadUrl(getIntent().getStringExtra("url"));

                webView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                        loader.setVisibility(View.VISIBLE);
                        offline.setVisibility(View.GONE);
                    }

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        loader.setVisibility(View.GONE);
                    }

                    @Override
                    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
//                        offline.setVisibility(View.VISIBLE);
                        sharebutton.setClickable(false);
//                        view.setBackgroundColor(Color.parseColor("#f7f7f7"));
                        Snackbar.make(view, "No Internet Connection, Please Try Again", Snackbar.LENGTH_LONG);
//                        webView.setVisibility(View.GONE);
                    }
                });
            }
        });

        printHashKey(getApplicationContext());
    }


    public void printHashKey(Context pContext) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("KEYHASH", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("KEYHASH", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("KEYHASH", "printHashKey()", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        if (!isOpen) {
            fbButton.setClickable(true);
            googleButton.setClickable(true);
            fbButton.startAnimation(fabOpen);
            googleButton.startAnimation(fabOpen);
            waButton.startAnimation(fabOpen);
            isOpen = true;
        } else {
            fbButton.setClickable(false);
            googleButton.setClickable(false);
            fbButton.startAnimation(fabClose);
            googleButton.startAnimation(fabClose);
            waButton.startAnimation(fabClose);

            isOpen = false;
        }
        switch (v.getId()) {
            case R.id.facebook:
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setContentUrl(Uri.parse(url)).build();

                if (shareDialog.canShow(ShareLinkContent.class)) {
                    shareDialog.show(linkContent);
                }
                break;

            case R.id.google:
                Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                        .setType("text/plain")
                        .setText(url)
                        .getIntent()
                        .setPackage("com.google.android.apps.plus");
                //startActivity(shareIntent);

                final String appPackageName = "com.google.android.apps.plus"; // getPackageName() from Context or Activity object
                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;

            case R.id.whatsapp:
                Intent intent1 = new Intent();
                intent1.setAction(Intent.ACTION_SEND);
                intent1.putExtra(Intent.EXTRA_TEXT, url);
                intent1.setType("text/plain");
                intent1.setPackage("com.whatsapp");
                // startActivity(intent1);

                final String appName = "com.whatsapp"; // getPackageName() from Context or Activity object
                try {
                    startActivity(intent1);
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appName)));
                }
                break;
        }
    }
}
