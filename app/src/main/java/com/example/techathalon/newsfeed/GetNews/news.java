package com.example.techathalon.newsfeed.GetNews;

public class news {

    private String id;
    private String name;
    private String author;
    private String title;
    private String description;
    private String url;
    private String publishAt;
    private String content;
    private String image_url;

    public news() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getPublishAt() {
        return publishAt;
    }

    public String getContent() {
        return content;
    }

    public String getImage_url() {
        return image_url;
    }


    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPublishAt(String publishAt) {
        this.publishAt = publishAt;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return "news{" +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", publishAt='" + publishAt + '\'' +
                ", content='" + content + '\'' +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}
