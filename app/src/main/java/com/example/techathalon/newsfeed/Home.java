package com.example.techathalon.newsfeed;

import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.techathalon.newsfeed.Database.DatabaseHelper;
import com.example.techathalon.newsfeed.Database.EditProfile;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.example.techathalon.newsfeed.GetNews.HorizontalNews;
import com.example.techathalon.newsfeed.GetNews.ViewNews;
import com.example.techathalon.newsfeed.GetNews.news;
import com.example.techathalon.newsfeed.GetNews.news_source;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    ActionBar ab;
    private SharedPrefConfig prefConfig;
    LayoutInflater inflater;
    TextView username;
    ImageView profile;
    byte[] userimage;
    String loginuser, useremail, personPhoto;;
    DatabaseHelper databaseHelper;
    boolean mobileconnectec, wificonnected;
    Bitmap btmp;
    ArrayList<news> newsList;
    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        prefConfig = new SharedPrefConfig(this);


        if (prefConfig.readLoginStatus()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.framelayout, new news_source()).commit();
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getApplicationContext(), gso);


//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        prefConfig.writeLoginStatus(true);
//                        startActivity(new Intent(getApplicationContext(), Home.class));
//                        Toast.makeText(getApplicationContext(), "Login success", Toast.LENGTH_SHORT).show();
//                        AccessToken accessToken = loginResult.getAccessToken();
//                        displayMessage(Profile.getCurrentProfile());
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        // App code
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        // App code
//                    }
//                });


        ab = getSupportActionBar();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.Open, R.string.Close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);
        databaseHelper = new DatabaseHelper(this);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navDrawer);
        View headerView = navigationView.getHeaderView(0);
        username = (TextView) headerView.findViewById((R.id.username));
        profile = (ImageView) headerView.findViewById((R.id.userimage));
        navigationView.setNavigationItemSelectedListener(this);

        if (prefConfig.readGoogleLoginStatus()==true) {
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
            if (acct != null) {
                loginuser = acct.getDisplayName();
                String personGivenName = acct.getGivenName();
                String personFamilyName = acct.getFamilyName();
                useremail = acct.getEmail();
                String personId = acct.getId();
                personPhoto = acct.getPhotoUrl().toString();
                Glide.with(this).load(personPhoto).into(profile);
            }
        } else {

            useremail = prefConfig.getLoggedInUser();

            Cursor cursor = databaseHelper.getUserData(useremail);
            if (cursor != null) {
                if (!useremail.equals("")) {
                    do {
                        loginuser = cursor.getString(1);
                        userimage = cursor.getBlob(7);
                    } while (cursor.moveToNext());
                }
            } else {
                loginuser = "";
            }


            try {
                btmp = BitmapFactory.decodeByteArray(userimage, 0, userimage.length);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (btmp == null) {
                profile.setImageResource(R.drawable.profilepic);
            } else {
                profile.setImageBitmap(btmp);
            }
        }

        username.setText(loginuser);


        //        checkConnection();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.setting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        prefConfig.writeLoginStatus(false);
                        prefConfig.writeGoogleLoginStatus(false);
                        Toast.makeText(getApplicationContext(), "Logged Out", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        prefConfig.clear();
                    }
                });
    }

    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                    }
                });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.homepage:
                getSupportFragmentManager().beginTransaction().addToBackStack("source").replace(R.id.framelayout, new news_source()).commit();
                break;

            case R.id.edit:
                if (prefConfig.readGoogleLoginStatus()) {
                    Toast.makeText(getApplicationContext(), "Create Your Account in News App", Toast.LENGTH_SHORT).show();
                } else {
                    getSupportFragmentManager().beginTransaction().addToBackStack("edit").replace(R.id.framelayout, new EditProfile()).commit();
                }
                break;

            case R.id.logout:
                if (prefConfig.readGoogleLoginStatus()) {
                    signOut();
                    revokeAccess();
                    prefConfig.writeGoogleLoginStatus(false);
                } else {
                    logout();
                }
                finish();
                break;

            case R.id.map:
                if (prefConfig.readGoogleLoginStatus()) {
                    Toast.makeText(getApplicationContext(), "Create Your Account in News App", Toast.LENGTH_SHORT).show();
                } else {
                    getSupportFragmentManager().beginTransaction().addToBackStack("map").replace(R.id.framelayout, new MapFragment()).commit();
                }
                break;

            case R.id.view_recycler:
                getSupportFragmentManager().beginTransaction().addToBackStack("map").replace(R.id.framelayout, new ViewNews()).commit();
                break;

            case R.id.save_offline:
                prefConfig.saveDataOffline(true);
                Toast.makeText(getApplicationContext(), "Articles you view will be saved offline", Toast.LENGTH_SHORT).show();
                break;

            case R.id.clear_cache:

                prefConfig.saveDataOffline(false);


                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                // builder.setTitle("Clear Cache");
                builder.setMessage("Clear Cached Articles?");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (databaseHelper.deleteJsonSource() && databaseHelper.deleteNewsSource()) {
                            Toast.makeText(getApplicationContext(), "Cache Cleared", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog dialog = builder.show();

                break;

            case R.id.reminder:
                getSupportFragmentManager().beginTransaction().addToBackStack("reminder").replace(R.id.framelayout, new Reminder()).commit();
                break;

        }
        drawerLayout.closeDrawers();
        return true;
    }

    @Override
    public void onBackPressed() {
        Fragment f = this.getSupportFragmentManager().findFragmentById(R.id.framelayout);

        if (f instanceof news_source || f instanceof EditProfile || f instanceof MapFragment || f instanceof ViewNews) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Exit");
            builder.setMessage(R.string.exit);

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog dialog = builder.show();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void logout() {
        prefConfig.writeLoginStatus(false);
        Toast.makeText(this, "Logged Out", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        prefConfig.clear();
    }

//    private void checkConnection() {
//        String status = "Not Connected to Internet";
//        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Service.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
//        if (networkInfo != null && networkInfo.isConnected()) {
//            wificonnected = networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
//            mobileconnectec = networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
//            if (wificonnected) {
//                status = "You are connected to WiFi";
//            } else if (mobileconnectec) {
//                status = "You are connected to Mobile Internet";
//            }
//            else{
//
//            }
//        }
//        Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
//
//    }

}












