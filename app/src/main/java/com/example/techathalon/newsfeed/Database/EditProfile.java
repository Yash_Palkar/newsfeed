package com.example.techathalon.newsfeed.Database;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.techathalon.newsfeed.GetNews.news_source;
import com.example.techathalon.newsfeed.MapFragment;
import com.example.techathalon.newsfeed.R;

import org.w3c.dom.Text;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

public class EditProfile extends Fragment implements View.OnClickListener {

    ImageView profilepic;
    EditText name, email, mobile, city;
    TextView changepassword, addimg, detect;
    Button update;
    String useremail;
    DatabaseHelper databaseHelper;
    SharedPrefConfig prefConfig;
    String userid, username, usermail, userpass, usermobile, userlatti, userlongi, useraddress;
    byte[] userimage;
    String lattitude, longitude, lattitude1, longitude1;
    private static final int REQUEST_LOCATION = 1;
    private static final int REQUEST_GALLERY = 999;
    private static final int CAPTURE = 3;
    private static final int SELECT = 4;
    LocationManager locationManager;
    Bitmap thumbnail;
    String addressLine, cityname, state, country, postalCode, knownName;
    double latti, longi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        profilepic = (ImageView) rootview.findViewById(R.id.profilepic);
        changepassword = (TextView) rootview.findViewById(R.id.changepass);
        name = (EditText) rootview.findViewById(R.id.name);
        email = (EditText) rootview.findViewById(R.id.email);
        mobile = (EditText) rootview.findViewById(R.id.mobile);
        city = (EditText) rootview.findViewById(R.id.location);
        update = (Button) rootview.findViewById(R.id.update);

        detect = (TextView) rootview.findViewById(R.id.detect);
        addimg = (TextView) rootview.findViewById(R.id.addimage);
        update.setOnClickListener(this);
        changepassword.setOnClickListener(this);
        detect.setOnClickListener(this);
        addimg.setOnClickListener(this);
        databaseHelper = new DatabaseHelper(getContext());


        prefConfig = new SharedPrefConfig(getContext());
        useremail = prefConfig.getLoggedInUser();

        Cursor cursor = databaseHelper.getUserData(useremail);
        if (cursor != null) {
            do {
                userid = cursor.getString(0);
                username = cursor.getString(1);
                usermail = cursor.getString(2);
                userpass = cursor.getString(3);
                usermobile = cursor.getString(4);
                userlatti = cursor.getString(5);
                userlongi = cursor.getString(6);
                userimage = cursor.getBlob(7);
            } while (cursor.moveToNext());

        }

        Geocoder geocoder;
        List<Address> addresses;

        geocoder = new Geocoder(getContext(), Locale.getDefault());

        if (userlatti == null || userlongi == null) {
            latti = 19.143804;
            longi = 72.868186;
        } else {
            latti = Double.parseDouble(userlatti);
            longi = Double.parseDouble(userlongi);
        }

        try {
            addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            addressLine = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            cityname = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getFeatureName();


        } catch (IOException e) {
            e.printStackTrace();
        }

        name.setText(username);
        email.setText(useremail);
        mobile.setText(usermobile);
        city.setText(cityname + ", " + state + ", " + country);


        Bitmap btmp = BitmapFactory.decodeByteArray(userimage, 0, userimage.length);
        if (btmp == null) {
            profilepic.setImageResource(R.drawable.profilepic);

        } else {
            profilepic.setImageBitmap(btmp);
        }

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            profilepic.setEnabled(false);
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            profilepic.setEnabled(true);
        }


        return rootview;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 10 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                //  txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public void UpdateData() {
        if (name.getText().toString().equals("") || email.getText().toString().equals("") || mobile.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Fields are empty", Toast.LENGTH_SHORT).show();
        } else {
            String e = userid;
            String e1 = name.getText().toString();
            String e2 = email.getText().toString();
            String e5 = mobile.getText().toString();
            String e6 = userlatti;
            String e7 = userlongi;
            profilepic.setDrawingCacheEnabled(true);
            profilepic.buildDrawingCache();
            Bitmap bitmap = profilepic.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] e8 = baos.toByteArray();


            if (isValidEmail(e2)) {

                if (isValidMobile(e5)) {

                    if (useremail.equals(e2)) {
                        boolean in = databaseHelper.updateData(e, e1, e2, e5, e6, e7, e8);
                        if (in == true) {
                            Toast.makeText(getContext(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                            getFragmentManager().beginTransaction().replace(R.id.framelayout, new news_source()).commit();

                        } else {
                            Toast.makeText(getContext(), "Some Error", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        boolean check = databaseHelper.checkAlreadyExist(e2);
                        if (check == true) {
                            boolean in = databaseHelper.updateData(e, e1, e2, e5, e6, e7, e8);
                            if (in == true) {
                                prefConfig.loggedInUserEmail(e2);
                                Toast.makeText(getContext(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                                getFragmentManager().beginTransaction().replace(R.id.framelayout, new MapFragment()).commit();

                            } else {
                                Toast.makeText(getContext(), "Some Error", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Email Alreay Exist", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getContext(), "Please Enter Valid Contact Number", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getContext(), "Please Enter Valid Email Address", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);


            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(getContext(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    addressLine = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    cityname = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                city.setText(cityname + ", " + state + ", " + country);

            } else if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(getContext(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    addressLine = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    cityname = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                city.setText(cityname + ", " + state + ", " + country);


            } else if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(getContext(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latti, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    addressLine = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    cityname = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                city.setText(cityname + ", " + state + ", " + country);

            } else {

                Toast.makeText(getContext(), "Unable to Trace your location", Toast.LENGTH_SHORT).show();

            }
        }

        lattitude1 = lattitude;
        longitude1 = longitude;
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public void addImage() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.uploadImages)
                .items(R.array.uploadImages)
                .itemsIds(R.array.itemIds)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                photoPickerIntent.setType("image/*");
                                startActivityForResult(photoPickerIntent, SELECT);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, CAPTURE);
                                break;
                            case 2:
                                profilepic.setImageResource(R.drawable.profilepic);
                                break;
                        }
                    }
                })
                .show();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT) {
            if (resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getActivity().getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    profilepic.setImageBitmap(selectedImage);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCapturedImageResult(data);
            }
        }
    }

    private void onCapturedImageResult(Intent data) {

        thumbnail = (Bitmap) data.getExtras().get("data");

        profilepic.setImageBitmap(thumbnail);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changepass:
                startActivity(new Intent(getContext(), ConfirmPassword.class));
                break;

            case R.id.update:
                UpdateData();
                break;

            case R.id.detect:
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();

                } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    getLocation();
                }
                break;

            case R.id.addimage:
                addImage();
                break;
        }
    }
}
