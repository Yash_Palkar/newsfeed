package com.example.techathalon.newsfeed;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.techathalon.newsfeed.Adapters.ViewPageAdapter;
import com.example.techathalon.newsfeed.Database.ForgotPassword;
import com.example.techathalon.newsfeed.Database.Login;
import com.example.techathalon.newsfeed.Database.Register;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;


/**
 * A simple {@link Fragment} subclass.
 */
public class user extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    ActionBar ab;
    private SharedPrefConfig sharedPrefConfig;

    public user() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_user, container, false);
        tabLayout = rootview.findViewById(R.id.tablayout);
        viewPager = rootview.findViewById(R.id.viewpager);

        ViewPageAdapter viewPageAdapter = new ViewPageAdapter(getChildFragmentManager());

        viewPageAdapter.AddFragment(new Login(), "Login");
        viewPageAdapter.AddFragment(new Register(), "Register");
        viewPageAdapter.AddFragment(new ForgotPassword(), "Recover");

        viewPager.setAdapter(viewPageAdapter);
        tabLayout.setupWithViewPager(viewPager);

        sharedPrefConfig = new SharedPrefConfig(getContext());
        return rootview;
    }

}
