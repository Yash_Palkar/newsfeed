package com.example.techathalon.newsfeed;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class Reminder extends Fragment implements View.OnClickListener {

    Button setButton, cancelButton;
    TextView todo;
    EditText message;
    private int notification_id = 1;

    final Calendar startTime = Calendar.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_reminder, container, false);

        setButton = (Button) rootview.findViewById(R.id.setReminder);
        cancelButton = (Button) rootview.findViewById(R.id.cancelReminder);
        todo = (TextView) rootview.findViewById(R.id.to_do);
        message = (EditText) rootview.findViewById(R.id.message_notify);

        todo.setOnClickListener(this);

        setButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        return rootview;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), AlarmReceiver.class);
        intent.putExtra("notification_id", notification_id);
        intent.putExtra("message", message.getText().toString());

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

        switch (v.getId()) {

            case R.id.to_do:

                final int hour = startTime.get(Calendar.HOUR_OF_DAY);
                int minute = startTime.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                todo.setText(hourOfDay + ":" + minute);
                                startTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                startTime.set(Calendar.MINUTE, minute);
                                startTime.set(Calendar.SECOND, 0);
                            }
                        }, hour, minute, false);
                timePickerDialog.show();
                break;

            case R.id.setReminder:

                long alarmStartTime = startTime.getTimeInMillis();

                alarmManager.set(AlarmManager.RTC_WAKEUP, alarmStartTime, alarmIntent);
                Snackbar.make(v, "Reminder Set", Snackbar.LENGTH_SHORT).show();
                message.setText("");
                break;


            case R.id.cancelReminder:
                alarmManager.cancel(alarmIntent);
                Snackbar.make(v, "Reminder Canceled", Snackbar.LENGTH_SHORT).show();
                break;
        }

    }
}


























