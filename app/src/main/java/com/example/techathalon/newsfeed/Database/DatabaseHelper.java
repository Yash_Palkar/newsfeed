package com.example.techathalon.newsfeed.Database;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.techathalon.newsfeed.GetNews.Connection;
import com.example.techathalon.newsfeed.GetNews.Sources;
import com.example.techathalon.newsfeed.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    Resources mResources;
    JSONObject jsonObject, object;
    JSONArray jsonArray;

    public static final String DATABASE_NAME = "user";
    public static final String TABLE_NAME = "user_details";
    public static final String col1 = "Id";
    public static final String col2 = "Name";
    public static final String col3 = "EMail";
    public static final String col4 = "Password";
    public static final String col5 = "Mobile";
    public static final String col6 = "Lattitude";
    public static final String col7 = "Longitude";
    public static final String col8 = "Profile";

    public static final String JSON_TABLE = "Source_Data";
    public static final String col_id = "Id";
    public static final String col_name = "Name";
    public static final String col_description = "Description";
    public static final String col_url = "Url";
    public static final String col_category = "Category";
    public static final String col_language = "Language";
    public static final String col_country = "Country";

    public static final String NEWS_TABLE = "News_Data";
    public static final String news_id = "Id";
    public static final String news_name = "Name";
    public static final String news_author = "Author";
    public static final String news_title = "Title";
    public static final String news_description = "Description";
    public static final String news_url = "Url";
    public static final String news_publishAt = "Date";
    public static final String news_content = "Content";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 8);
        mResources = context.getResources();
        SQLiteDatabase db = this.getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, EMAIL TEXT, PASSWORD TEXT, MOBILE TEXT, LATTITUDE TEXT, LONGITUDE TEXT, PROFILE BLOB)";
        String create_source = "create table " + JSON_TABLE + " (ID TEXT,  Name TEXT, Description TEXT, Url TEXT, Category TEXT, Language TEXT, Country TEXT)";
        String create_news = "create table " + NEWS_TABLE + " (ID TEXT, NAME TEXT, Author TEXT,  Title TEXT, Description TEXT, Url TEXT, Date TEXT, Content TEXT)";

        db.execSQL(create);
        db.execSQL(create_source);
        db.execSQL(create_news);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + JSON_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + NEWS_TABLE);
        onCreate(db);
    }

    public boolean insert(String name, String email, String password, String mobile, String latt, String longi, byte[] img) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(col2, name);
        contentValues.put(col3, email);
        contentValues.put(col4, password);
        contentValues.put(col5, mobile);
        contentValues.put(col6, latt);
        contentValues.put(col7, longi);
        contentValues.put(col8, img);

        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateData(String id, String name, String email, String mobile, String latt, String longi, byte[] img) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(col1, id);
        contentValues.put(col2, name);
        contentValues.put(col3, email);
        contentValues.put(col5, mobile);
        contentValues.put(col6, latt);
        contentValues.put(col7, longi);
        contentValues.put(col8, img);

//        String tabledata = tableToString(db, TABLE_NAME);
//        Log.d("Tabledetails", tabledata);


        // long result = db.update(TABLE_NAME, null, contentValues);
        long result = db.update(TABLE_NAME, contentValues, " Id = ?", new String[]{id});
        return true;
    }


    public boolean checkAlreadyExist(String email) {

        SQLiteDatabase db = getReadableDatabase();
        String query = "Select " + col3 + " from " + TABLE_NAME + " where " + col3 + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{email});
        if (cursor.getCount() > 0) {
            return false;
        } else
            return true;
    }

    public boolean checkemail(String email) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select " + col3 + " from " + TABLE_NAME + " where " + col3 + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{email});
        if (cursor.getCount() > 0) {
            return true;
        } else
            return false;
    }


    public boolean checklogin(String email, String passwword) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select * from " + TABLE_NAME + " where " + col3 + " = ? and " + col4 + " =?";
        Cursor cursor = db.rawQuery(query, new String[]{email, passwword});

//        String tabledata = tableToString(db, TABLE_NAME);
//        Log.d("Tabledetails", tabledata);

        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void getName(String email) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select " + col2 + " from " + TABLE_NAME + " where " + col3 + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{email});
        return;
    }

    public boolean resetPasswrod(String email, String passwrod) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(col4, passwrod);
        db.update(TABLE_NAME, values, col3 + " = ? ", new String[]{email});
        db.close();
        return true;
    }


    public List<String> getLocation(String email) {
        SQLiteDatabase db = getReadableDatabase();
        List<String> list = new ArrayList<String>();
        String countQuery = "SELECT * FROM user_details  WHERE EMail= '" + email + "'";
        Cursor res = db.rawQuery(countQuery, null);

        res.moveToFirst();
        while (!res.isAfterLast()) {

            String s1 = (res.getString(5));
            String s2 = (res.getString(6));
            list.add(s1);
            list.add(s2);

            res.moveToNext();
        }
        res.close();
        return list;
    }


    public Cursor getUserData(String email) {
        SQLiteDatabase db = getReadableDatabase();
        String select = "SELECT * FROM user_details  WHERE EMail= '" + email + "'";
        Cursor cursor = db.rawQuery(select, null);
        cursor.moveToFirst();

        return cursor;
    }

    public boolean deleteJsonSource() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(JSON_TABLE, null, null);
        return true;
    }

    public boolean writeDatatoDb(String Id, String Name, String Description, String Url, String Category, String Language, String Country) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(col_id, Id);
        contentValues.put(col_name, Name);
        contentValues.put(col_description, Description);
        contentValues.put(col_url, Url);
        contentValues.put(col_category, Category);
        contentValues.put(col_language, Language);
        contentValues.put(col_country, Country);

        long result = db.insert(JSON_TABLE, null, contentValues);
        Log.d("my_json", "Inserted = " + contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getJsonData() {
        SQLiteDatabase db = getReadableDatabase();
        String select = "SELECT * FROM " + JSON_TABLE;
        Cursor cursor = db.rawQuery(select, null);
        cursor.moveToFirst();
        return cursor;
    }

    public boolean deleteNewsSource() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(NEWS_TABLE, null, null);
        return true;
    }


    public boolean writeNewstoDb(String Id, String Name, String Author, String Title, String Description, String Url, String Date, String Content) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(news_id, Id);
        contentValues.put(news_name, Name);
        contentValues.put(news_author, Author);
        contentValues.put(news_title, Title);
        contentValues.put(news_description, Description);
        contentValues.put(news_url, Url);
        contentValues.put(news_publishAt, Date);
        contentValues.put(news_content, Content);

        long result = db.insert(NEWS_TABLE, null, contentValues);
        Log.d("my_news", "Inserted = " + contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getNewsData() {
        SQLiteDatabase db = getReadableDatabase();
        String select = "SELECT * FROM " + NEWS_TABLE;
        Cursor cursor = db.rawQuery(select, null);
        cursor.moveToFirst();
        return cursor;
    }

    public String tableToString(String tableName) {
        Log.d("TABLE", "tableToString called");
        String tableString = String.format("Table %s:\n", tableName);
        Cursor allRows = getReadableDatabase().rawQuery("SELECT * FROM " + JSON_TABLE, null);
        // tableString += cursorToString(allRows);
        tableString += cursorToString(allRows);
        Log.d("TABLE", tableString);
        return tableString;
    }

    public String cursorToString(Cursor cursor) {
        String cursorString = "";
        if (cursor.moveToFirst()) {
            String[] columnNames = cursor.getColumnNames();
            for (String name : columnNames)
                cursorString += String.format("%s ][ ", name);
            cursorString += "\n";
            do {
                for (String name : columnNames) {
                    cursorString += String.format("%s ][ ",
                            cursor.getString(cursor.getColumnIndex(name)));
                }
                cursorString += "\n";
            } while (cursor.moveToNext());
        }
        return cursorString;
    }

}
