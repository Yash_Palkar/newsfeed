package com.example.techathalon.newsfeed;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.techathalon.newsfeed.Adapters.ViewPageAdapter;
import com.example.techathalon.newsfeed.Database.ForgotPassword;
import com.example.techathalon.newsfeed.Database.Login;
import com.example.techathalon.newsfeed.Database.Register;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.example.techathalon.newsfeed.GetNews.news_source;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.viewpager);

        SharedPrefConfig prefConfig = new SharedPrefConfig(this);

        if (prefConfig.readLoginStatus()) {
            Intent intent = new Intent(this, Home.class);
            startActivity(intent);
            finish();
        }
        ViewPageAdapter viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager());

        viewPageAdapter.AddFragment(new Login(), "Login");
        viewPageAdapter.AddFragment(new Register(), "Register");
        viewPageAdapter.AddFragment(new ForgotPassword(), "Recover");

        viewPager.setAdapter(viewPageAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exit");
        builder.setMessage(R.string.exit);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.show();
    }
}
