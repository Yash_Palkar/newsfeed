package com.example.techathalon.newsfeed.Database;


import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.techathalon.newsfeed.Home;
import com.example.techathalon.newsfeed.MainActivity;
import com.example.techathalon.newsfeed.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

public class Login extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private SharedPrefConfig prefConfig;

    private static final String EMAIL = "email";
    private static final String PROFILE = "public_profile";

    EditText email, password;
    LoginButton loginButton;
    Button login;
    TextView signup, forgot;
    CheckBox chk;
    DatabaseHelper databaseHelper;
    CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    ProfileTracker profileTracker;

    SignInButton signInButton;
    FirebaseAuth firebaseAuth;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 2;
    GoogleApiClient googleApiClient;
    FirebaseAuth.AuthStateListener authStateListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootview = inflater.inflate(R.layout.fragment_login, container, false);

        prefConfig = new SharedPrefConfig(getContext());

        databaseHelper = new DatabaseHelper(getContext());

        email = rootview.findViewById(R.id.email);
        password = rootview.findViewById(R.id.password);

        login = rootview.findViewById(R.id.loginbutton);
        signup = rootview.findViewById(R.id.signup);
        forgot = rootview.findViewById(R.id.forgot);
        chk = (CheckBox) rootview.findViewById(R.id.check);
        //loginButton = (LoginButton) rootview.findViewById(R.id.login_facebook);
        signInButton = (SignInButton) rootview.findViewById(R.id.login_google);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        chk.setOnCheckedChangeListener(this);

        login.setOnClickListener(this);
        signup.setOnClickListener(this);
        forgot.setOnClickListener(this);
        signInButton.setOnClickListener(this);


// Login with firebase methods

        firebaseAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();
        //Sign in with google plus

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);


        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    prefConfig.writeLoginStatus(true);
                    startActivity(new Intent(getContext(), Home.class));
                }
            }
        };
        // google plus sign in ends here


        //Login with facebook starts here

        // Initialize Facebook Login button
        callbackManager = CallbackManager.Factory.create();
     //   loginButton.setReadPermissions("email", "public_profile");
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.d("TAG", "facebook:onSuccess:" + loginResult);
//                handleFacebookToken(loginResult.getAccessToken());
//            }
//
//            @Override
//            public void onCancel() {
//                Log.d("TAG", "facebook:onCancel");
//                // ...
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.d("TAG", "facebook:onError", error);
//                // ...
//            }
//        });


        //login with facebook ends here

// login with firebase methods ends here

        return rootview;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if(user != null){
            updateUI();
        }
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    private void updateUI() {
        Toast.makeText(getActivity(), "Login Successfully", Toast.LENGTH_SHORT).show();
        prefConfig.writeLoginStatus(true);
        prefConfig.writeGoogleLoginStatus(true);
        Intent loginIntent = new Intent(getContext(), Home.class);
        startActivity(loginIntent);
        Toast.makeText(getActivity(), "Login success", Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    public void loginwithFB() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(EMAIL, PROFILE));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(), "Login Canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handleFacebookToken(AccessToken accessToken) {
        Log.d("TAG", "handleFacebookAccessToken:" + accessToken);

        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener((Executor) this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("tag", "signInWithCredential:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
     //   Toast.makeText(getActivity(), "Login Success with FB", Toast.LENGTH_SHORT).show();

        if (requestCode == RC_SIGN_IN) {
            prefConfig.writeLoginStatus(true);
            prefConfig.writeGoogleLoginStatus(true);
            Intent loginIntent = new Intent(getContext(), Home.class);
            startActivity(loginIntent);
            Toast.makeText(getActivity(), "Login success", Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }
    }

    public void login() {
        if (email.getText().toString().equals("") || password.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Fields are empty", Toast.LENGTH_SHORT).show();
        } else {
            String e1 = email.getText().toString();
            String e2 = password.getText().toString();

            boolean checkmail = databaseHelper.checkemail(e1);
            if (checkmail == true) {
                boolean log = databaseHelper.checklogin(e1, e2);
                if (log == true) {

                    Toast.makeText(getContext(), "Successfully Logged In", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getContext(), Home.class));
                    getActivity().finish();
                    prefConfig.writeGoogleLoginStatus(false);
                    prefConfig.writeLoginStatus(true);
                    prefConfig.loggedInUserEmail(e1);

                    String latti, longi;
                    List<String> arr = databaseHelper.getLocation(e1);

                    latti = arr.get(0);
                    longi = arr.get(1);

                    if (latti == null && longi == null) {
                        prefConfig.lattitude(String.valueOf(0.0));
                        prefConfig.longitude(String.valueOf(0.0));
                    } else {
                        prefConfig.lattitude(latti);
                        prefConfig.longitude(longi);
                    }
                    email.setText("");
                    password.setText("");

                } else {
                    Toast.makeText(getContext(), "Enter Correct Password", Toast.LENGTH_SHORT).show();
                    password.setText("");
                }
            } else {
                Toast.makeText(getContext(), "No such email exixts", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginbutton:
                login();
                break;

            case R.id.signup:
                ViewPager viewPager = (ViewPager) getActivity().findViewById(
                        R.id.viewpager);
                viewPager.setCurrentItem(1);
                break;

            case R.id.forgot:
                ViewPager viewPager1 = (ViewPager) getActivity().findViewById(R.id.viewpager);
                viewPager1.setCurrentItem(2);
                break;

            case R.id.login_google:
                signIn();
                break;

            case R.id.login_facebook:
                loginwithFB();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
    }
}
