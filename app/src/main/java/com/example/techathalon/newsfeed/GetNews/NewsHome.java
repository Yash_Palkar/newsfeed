package com.example.techathalon.newsfeed.GetNews;


import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.techathalon.newsfeed.Adapters.newsAdapter;
import com.example.techathalon.newsfeed.Database.DatabaseHelper;
import com.example.techathalon.newsfeed.Database.SharedPrefConfig;
import com.example.techathalon.newsfeed.R;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.florent37.diagonallayout.DiagonalLayout;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewsHome extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, SearchView.OnQueryTextListener {


    private static final String API_KEY = "326b273af74c4a80913bca7789f2d898";
    public String SOURCES = "";
    JSONObject jsonObject;
    JSONObject object, object1;
    JSONArray jsonArray, jsonArray1;
    // public String url = "https://newsapi.org/v2/top-headlines?country=us&apiKey=" + API_KEY;
    public String url = "https://newsapi.org/v2/top-headlines?sources=" + SOURCES + "&apiKey=" + API_KEY;
    private RecyclerView recyclerView;
    ArrayList<news> newsList;
    ProgressDialog progressDialog;
    SwipeRefreshLayout swipeRefreshLayout;
    KenBurnsView kbv;
    TextView topAutor, topTitle, NoConnection, NoConnection1;
    DiagonalLayout diagonalLayout;
    newsAdapter adapter;
    news mNews;
    String diagonalUrl = "";
    Toolbar toolbar;
    ArrayList<news> arrayList;
    DatabaseHelper databaseHelper;
    SearchView searchView;
    MaterialSearchView materialSearchView;
    SharedPrefConfig sharedPrefConfig;

    private StringRequest request;

    private RequestQueue requestQueue;
    private JsonArrayRequest jsonArrayRequest;

    String article_id, article_name;

    String news_id = "", news_name = "", news_author = "", news_title = "", news_description = "", news_url = "", news_publishAt = "", news_content = "";

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.fragment_news_home, container, false);

        databaseHelper = new DatabaseHelper(getContext());
        databaseHelper.tableToString(DatabaseHelper.NEWS_TABLE);
        sharedPrefConfig = new SharedPrefConfig(getContext());

        toolbar = (Toolbar) rootview.findViewById(R.id.searchbar);

        swipeRefreshLayout = (SwipeRefreshLayout) rootview.findViewById(R.id.swipeRefresh);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerView);
        newsList = new ArrayList<>();

        adapter = new newsAdapter(getActivity(), newsList);

        NoConnection = (TextView) rootview.findViewById(R.id.no_connection);
        NoConnection1 = (TextView) rootview.findViewById(R.id.no_connection1);

        materialSearchView = (MaterialSearchView) rootview.findViewById(R.id.searchView);
        materialSearchView.closeSearch();
        //  materialSearchView.setSuggestions(newsList);
        materialSearchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (null != adapter) {
                    if (TextUtils.isEmpty(s)) {
                        adapter.getFilter().filter("");
                    } else {
                        adapter.getFilter().filter(s);
                    }
                    adapter.notifyDataSetChanged();
                }
                return false;
            }
        });

        NoConnection.setVisibility(View.GONE);
        NoConnection1.setVisibility(View.GONE);

        searchView = (SearchView) rootview.findViewById(R.id.searchView1);

        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        searchEditText.setHintTextColor(getResources().getColor(R.color.lightgray));
        searchView.setBackgroundColor(getResources().getColor(R.color.white));

        //to change the magnifying glass on keyboard to submit icon
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setQueryHint("Search...");
        searchView.setOnQueryTextListener(this);
        searchView.setBackgroundResource(R.drawable.roundspinner);

        Bundle bundle = getArguments();
        if (bundle.get("Source_Url") != null) {
            SOURCES = bundle.getString("Source_Url");
        } else {
            SOURCES = bundle.getString("link");
        }

        mNews = new news();
        diagonalLayout = (DiagonalLayout) rootview.findViewById(R.id.diagonal);
        kbv = (KenBurnsView) rootview.findViewById(R.id.kenburns);

        topTitle = (TextView) rootview.findViewById(R.id.top_title);

        if (Connection.isNetworkAvailable(getContext())) {

            //  databaseHelper.deleteNewsSource();
            Toast.makeText(getContext(), "You are connected To Internet", Toast.LENGTH_SHORT).show();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading News...");
            progressDialog.show();
            ArticleResponse();
        } else {
            final Snackbar snackbar = Snackbar.make(container, "No Internet Connection", Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("REFRESH", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connection.isNetworkAvailable(getContext())) {
                        progressDialog = new ProgressDialog(getContext());
                        progressDialog.setMessage("Loading News...");
                        progressDialog.show();
                        ArticleResponse();
                    } else {
                        Toast.makeText(getContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();

                    }
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
            // Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            // readFromInternalStorage();
            Cursor cursor = databaseHelper.getNewsData();

            if (cursor != null && cursor.moveToFirst()) {
                do {
                    news mNews = new news();

                    mNews.setId(cursor.getString(0));
                    mNews.setName(cursor.getString(1));
                    mNews.setAuthor(cursor.getString(2));
                    mNews.setTitle(cursor.getString(3));
                    mNews.setDescription(cursor.getString(4));
                    mNews.setUrl(cursor.getString(5));
                    mNews.setPublishAt(cursor.getString(6));
                    mNews.setContent(cursor.getString(7));

                    article_id = mNews.getId();
                    article_name = mNews.getName();

                    newsList.add(mNews);
                }
                while (cursor.moveToNext());



                    if (SOURCES.equals(article_id)) {
                        newsAdapter adapter = new newsAdapter(getContext(), newsList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        diagonalLayout.setVisibility(View.GONE);
                        NoConnection.setVisibility(View.VISIBLE);
                        NoConnection1.setVisibility(View.VISIBLE);
                    }


            }
        }


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()

    {
        @Override
        public void onRefresh () {
        if (Connection.isNetworkAvailable(getContext())) {

            diagonalLayout.setVisibility(View.VISIBLE);
            NoConnection.setVisibility(View.GONE);
            NoConnection1.setVisibility(View.GONE);

            newsList.clear();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading News...");
            progressDialog.show();
            ArticleResponse();

            swipeRefreshLayout.setRefreshing(false);
            adapter.notifyDataSetChanged();
        } else {
            final Snackbar snackbar = Snackbar.make(container, "No Internet Connection", Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("REFRESH", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Connection.isNetworkAvailable(getContext())) {
                        progressDialog = new ProgressDialog(getContext());
                        progressDialog.setMessage("Loading News...");
                        progressDialog.show();
                        ArticleResponse();
                    } else {
                        Toast.makeText(getContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                        snackbar.show();
                    }
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
            swipeRefreshLayout.setRefreshing(false);
        }
    }
    });

        diagonalLayout.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View v){
        Intent intent = new Intent(getContext(), DetailsWebView.class);
        intent.putExtra("url", diagonalUrl);
        v.getContext().startActivity(intent);
    }
    });

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this);
        new

    ItemTouchHelper(itemTouchHelperCallback).

    attachToRecyclerView(recyclerView);

    arrayList =new ArrayList<news>();

    ArticleResponse();

        return rootview;
}


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof newsAdapter.newsHolder) {
            // get the removed item name to display it in snack bar
            String name = newsList.get(viewHolder.getAdapterPosition()).getTitle();

            // backup of removed item for undo purpose
            final news deletedItem = newsList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            adapter.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(getView(), name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    adapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.sorting_menu, menu);
        MenuItem item = menu.findItem(R.id.search);
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void ArticleResponse() {
        url = "https://newsapi.org/v2/top-headlines?sources=" + SOURCES + "&apiKey=" + API_KEY;

        requestQueue = Volley.newRequestQueue(getContext());

        request = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONArray articleArray = obj.getJSONArray("articles");
                    newsList = new ArrayList<>();

//                    if (!sharedPrefConfig.readDataOffline()) {
                      databaseHelper.deleteNewsSource();
//                    }

                    for (int i = 0; i < articleArray.length(); i++) {

                        mNews = new news();

                        JSONObject jsonobject = articleArray.getJSONObject(i);

                        JSONObject source = jsonobject.getJSONObject("source");

                        news_id = source.getString("id");
                        news_name = source.getString("name");

                        // diagonal layout
                        // topAutor.setText(articleArray.getJSONObject(0).getString("author"));
                        topTitle.setText(articleArray.getJSONObject(0).getString("title"));

                        if (getContext() != null) {
                            Glide.with(getContext()).load(articleArray.getJSONObject(0).getString("urlToImage")).into(kbv);
                        }

                        sharedPrefConfig.newsImageUrl(articleArray.getJSONObject(0).getString("urlToImage"));

                        //diagonal layout

                        mNews.setId(source.getString("id"));
                        mNews.setName(source.getString("name"));
                        mNews.setTitle(jsonobject.getString("title"));
                        mNews.setDescription(jsonobject.getString("description"));
                        mNews.setAuthor(jsonobject.getString("author"));
                        mNews.setUrl(jsonobject.getString("url"));
                        mNews.setImage_url(jsonobject.getString("urlToImage"));
                        mNews.setPublishAt(jsonobject.getString("publishedAt"));
                        mNews.setContent(jsonobject.getString("content"));

                        //title[i] = object.getString("title");
                        diagonalUrl = articleArray.getJSONObject(0).getString("url");

                        //to add in sqlite database
                        news_author = jsonobject.getString("author");
                        news_title = jsonobject.getString("title");
                        news_description = jsonobject.getString("description");
                        news_url = jsonobject.getString("url");
                        news_publishAt = jsonobject.getString("publishedAt");
                        news_content = jsonobject.getString("content");

                        databaseHelper.writeNewstoDb(news_id, news_name, news_author, news_title, news_description, news_url, news_publishAt, news_content);
                        //sqlite databse added
                        newsList.add(mNews);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
                }

                adapter = new newsAdapter(getActivity(), newsList);

                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                progressDialog.cancel();

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
            }
        });

        requestQueue.add(request);

    }

//    public class JsonResponse extends AsyncTask<String, Void, String> {
//
//        @Override
//        protected String doInBackground(String... args) {
//            String xml = "";
//            String urlParams = "";
//            url = "https://newsapi.org/v2/top-headlines?sources=" + SOURCES + "&apiKey=" + API_KEY;
//            xml = Connection.executeGet(url);
//            return xml;
//        }
//
//        @Override
//        protected void onPostExecute(String xml) {
//            String imageUrl = null;
//            //    newsList.clear();
//
//            if (xml.length() > 10) //check if empty
//            {
//                try {
//                    jsonObject = new JSONObject(xml);
//                    jsonArray = jsonObject.getJSONArray("articles");
//                    databaseHelper.deleteNewsSource();
//
//                    for (int i = 0; i < jsonArray.length(); i++) {
//
//                        mNews = new news();
//                        object = jsonArray.getJSONObject(i);
////
////                        jsonArray1 = jsonObject.getJSONArray("source");
////
////                        for (int j=0; j<jsonArray1.length(); j++){
////
////                            object1 = jsonArray1.getJSONObject(i);
////                            mNews.setId(object1.getString("id"));
////                            mNews.setName(object1.getString("name"));
////                        }
//
//                        // diagonal layout
//                        topAutor.setText(jsonArray.getJSONObject(0).getString("author"));
//                        topTitle.setText(jsonArray.getJSONObject(0).getString("title"));
//
//                        if (getContext() != null) {
//                            Glide.with(getContext()).load(jsonArray.getJSONObject(0).getString("urlToImage")).into(kbv);
//                        }
//
//                        sharedPrefConfig.newsImageUrl(jsonArray.getJSONObject(0).getString("urlToImage"));
//
//                        //diagonal layout
//
//                        mNews.setTitle(object.getString("title"));
//                        mNews.setDescription(object.getString("description"));
//                        mNews.setAuthor(object.getString("author"));
//                        mNews.setUrl(object.getString("url"));
//                        mNews.setImage_url(object.getString("urlToImage"));
//                        mNews.setPublishAt(object.getString("publishedAt"));
//                        mNews.setContent(object.getString("content"));
//
//                        //title[i] = object.getString("title");
//                        diagonalUrl = jsonArray.getJSONObject(0).getString("url");
//
//                        //to add in sqlite database
//                        news_author = object.getString("author");
//                        news_title = object.getString("title");
//                        news_description = object.getString("description");
//                        news_url = object.getString("url");
//                        news_publishAt = object.getString("publishedAt");
//                        news_content = object.getString("content");
//
//                        databaseHelper.writeNewstoDb(news_id, news_name, news_author, news_title, news_description, news_url, news_publishAt, news_content);
//                        //sqlite databse added
//                        newsList.add(mNews);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getContext(), "Unexpected error", Toast.LENGTH_SHORT).show();
//                }
//                HorizontalNews newFragment = new HorizontalNews();
//                Bundle bundle = new Bundle();
//                bundle.putString("imageLink", imageUrl);
//                newFragment.setArguments(bundle);
//
////                Cursor cursor = databaseHelper.getNewsData();
////                if (cursor != null && cursor.moveToFirst()) {
////                    do {
////                        news mNews = new news();
////                        mNews.setAuthor(cursor.getString(0));
////                        mNews.setTitle(cursor.getString(1));
////                        mNews.setDescription(cursor.getString(2));
////                        mNews.setUrl(cursor.getString(3));
////                        mNews.setPublishAt(cursor.getString(4));
////                        mNews.setContent(cursor.getString(5));
////
////                        newsList.add(mNews);
////                    }
////                    while (cursor.moveToNext());
////            }
//                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                recyclerView.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//                progressDialog.cancel();
//            } else
//
//            {
//                Toast.makeText(getContext(), "No news found", Toast.LENGTH_SHORT).show();
//            }
//        }
//
//    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (null != adapter) {
            if (TextUtils.isEmpty(s)) {
                adapter.getFilter().filter("");
            } else {
                adapter.getFilter().filter(s);
            }
            adapter.notifyDataSetChanged();
        }
        return false;
    }
}